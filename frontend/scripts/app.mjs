import { loginPage } from "./login.mjs";
import { generateGUI } from "./dashboard.mjs";

const loginPageGUI = loginPage();

export const nextPage = data => {
  //   console.log(players);
  const playersPageGUI = generateGUI(data);
  document.body.querySelector("section").remove();

  document.body.appendChild(playersPageGUI);
};

document.body.appendChild(loginPageGUI);

//dom: walidacja inputa
