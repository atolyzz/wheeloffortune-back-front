export const createTag = ({
  tagName = "div",
  tagText,
  className,
  evt,
  idName
}) => {
  const tag = document.createElement(tagName);
  if (tagText !== undefined) {
    const text = document.createTextNode(tagText);
    tag.appendChild(text);
  }

  if (className) {
    if (Array.isArray(className)) {
      className.forEach(cl => tag.classList.add(cl));
    } else {
      tag.classList.add(className);
    }
  }

  if (evt) {
    tag.addEventListener(evt.type, evt.cb);
  }

  if (idName) {
    tag.id = idName;
  }

  return tag;
};
