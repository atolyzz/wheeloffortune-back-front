import { createTag } from "./helpers.mjs";
import { nextPage } from "./app.mjs";

export const loginPage = () => {
  const loginWrapper = createTag({
    tagName: "section",
    className: "box-login"
  });

  const title = createTag({
    tagName: "h1",
    tagText: "Koło Fortuny",
    className: "hdl-primary"
  });

  const loginForm = createTag({ tagName: "form" });
  const nameInput = createTag({ tagName: "input" });
  const buttonForm = createTag({
    tagName: "button",
    tagText: "zaloguj się",
    evt: {
      type: "click",
      cb: event => {
        event.preventDefault();
        console.log("zalogowano");
        signIn(nameInput.value);
      }
    }
  });

  loginForm.appendChild(nameInput);
  loginForm.appendChild(buttonForm);

  loginWrapper.appendChild(title);
  loginWrapper.appendChild(loginForm);
  return loginWrapper;
};

const signIn = userName => {
  axios
    .post("http://localhost:4000/login", {
      user: userName
    })
    .then(response => nextPage(response.data))
    .catch(error => console.log(error));
};
