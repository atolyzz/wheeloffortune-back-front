import { createTag } from "./helpers.mjs";

export const generateGUI = ({ users: players, sentence }) => {
  const board = createTag({ tagName: "section" });

  //players
  const playersWrapper = createTag({});
  const playersBox = generatePlayers(players);
  playersWrapper.appendChild(playersBox);
  board.appendChild(playersWrapper);

  //koło
  const wheelWrapper = generateWheel();
  board.appendChild(wheelWrapper);

  //hasła
  const wrapper = createTag({ tagName: "div" });
  const sentenceBox = generateSentenceBoard(sentence);
  wrapper.appendChild(sentenceBox);
  board.appendChild(wrapper);

  return board;
};

const generatePlayers = players => {
  const playersBox = createTag({
    tagName: "section",
    className: "box-players"
  });

  players.forEach(player => {
    const playerBox = createTag({
      className: player.active ? "active" : ""
    });

    const playerName = createTag({ tagName: "h2", tagText: player.name });
    const playerScore = createTag({ tagName: "span", tagText: player.score });

    playerBox.appendChild(playerName);
    playerBox.appendChild(playerScore);
    playersBox.appendChild(playerBox);
  });

  return playersBox;
};

const generateSentenceBoard = sentence => {
  const sentenceBox = createTag({ className: "box-letter" });

  sentence.split("").forEach(letter => {
    const letterBox = createTag({
      tagName: "span",
      className: letter === "?" ? ["space", "letter"] : "letter",
      tagText: letter
    });
    sentenceBox.appendChild(letterBox);
  });

  return sentenceBox;
};

const generateWheel = () => {
  const wheelBox = createTag({ tagName: "div" });
  const wheelResult = createTag({ tagName: "p" });
  const wheelButton = createTag({
    tagName: "button",
    tagText: "kręć",
    evt: {
      type: "click",
      cb: () => {
        axios.get("http://localhost:4000/wheel").then(response => {
          const wheelSpinResult = response.data.wheel;
          let message;
          switch (wheelSpinResult) {
            case 0:
              message = "Bankrut!";
              // updatePlayerScore(0, 0);
              // nextPlayer();
              break;
            case -1:
              message = "Strata kolejki!";
              // nextPlayer();
              break;
            default:
            // formInput.disabled = false;
            // wheelButton.disabled = true;
          }
          wheelResult.innerText =
            wheelSpinResult === -1 || wheelSpinResult === 0
              ? message
              : wheelSpinResult;
          // playersWrapper.querySelector(".box-players").remove();
          // playersWrapper.appendChild(generatePlayers());
        });
      }
    }
  });
  wheelBox.appendChild(wheelResult);
  wheelBox.appendChild(wheelButton);

  return wheelBox;
};
