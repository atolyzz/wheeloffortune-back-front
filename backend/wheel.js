const wheelLabels = [200, 500, 1000, 2000, 5000, 10000, 0, -1];

exports.getRandomLabels = () => {
  return wheelLabels[Math.floor(Math.random() * wheelLabels.length)];
};
