//zaimportowanie za pomocą commonJS
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const board = require("./board.js");
const wheel = require("./wheel.js");

const app = express();

const corsOptions = { origin: "http://localhost:5500" };

//middleware:
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const users = [
  {
    name: "Paweł",
    score: 0,
    active: true
  },
  {
    name: "Aneta",
    score: 0,
    active: false
  },
  {
    name: "Maciek",
    score: 0,
    active: false
  }
];

//ustalamy że na danym endpointcie jest dana metoda (tutaj post)
app.post("/login", function(req, res) {
  //   console.log(req);
  const sentence = board.chooseSentence();
  const activeUser = req.body.user;
  users[0].name = activeUser;

  res.send(JSON.stringify({ users, sentence }));
});

app.get("/wheel", function(req, res) {
  res.send(JSON.stringify({ wheel: wheel.getRandomLabels() }));
});

console.log("server działa na porcie 4000");
app.listen(4000);
