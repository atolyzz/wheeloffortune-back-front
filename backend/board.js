// import { nextPlayer, updatePlayerScore } from "./players.mjs";
// import { activeScore } from "./wheel.mjs";
let activeSentence;
exports.activeSentenceForDashboard;

const sentences = [
  {
    sentence: "Jak Kuba Bogu tak Bóg Kubańczykom",
    category: "przysłowia"
  },
  {
    sentence: "Baba z wozu koniom lżej",
    category: "powiedznie"
  },
  {
    sentence: "Nosił wilk razy kilka ponieśli i wilka",
    category: "przysłowia"
  }
];

exports.chooseSentence = () => {
  activeSentence =
    sentences[Math.floor(Math.random() * sentences.length)].sentence;
  const temp = generateSentenceForUser();
  return temp;
};

const generateSentenceForUser = () => {
  activeSentenceForDashboard = activeSentence
    .split("")
    .map(letter => (letter === " " ? "?" : " "))
    .join("");

  return activeSentenceForDashboard;
};

const checkLetter = letter => {
  const timesOfLetter = activeSentenceForDashboard
    .split("")
    .filter(
      (el, idx) =>
        activeSentence[idx].toLowerCase() === letter.toLowerCase() &&
        el.toLowerCase() !== letter.toLowerCase()
    );

  if (timesOfLetter.length) {
    activeSentenceForDashboard = activeSentenceForDashboard
      .split("")
      .map((el, idx) =>
        activeSentence[idx].toLowerCase() === letter.toLowerCase()
          ? activeSentence[idx]
          : el
      )
      .join("");
    // updatePlayerScore(activeScore, timesOfLetter.length);
  } else {
    // nextPlayer();
  }
};
